1)	Menu Button Click - Short Click/Snap Perc (waveplay_old) - https://freesound.org/people/waveplay_old/sounds/399934/
2)	Menu Button Hovering - Switch Flip #1 (IanStarGem) - https://freesound.org/people/IanStarGem/sounds/278205/
3)	Menu Music - Short Medieval Loop (remaxim) - https://opengameart.org/content/short-medieval-loop
4)	Crushing a person with a stone – Stone fall to the ground + Crushing man
5)	 Stone fall to the ground - Stone dropping (alegemaate) - https://freesound.org/people/alegemaate/sounds/364711/
6)	Death Scream - wilhelm (SweetNeo85) - https://freesound.org/people/SweetNeo85/sounds/13797/
7)	Crushing man - 2 Impact Wet (original_sound) - https://freesound.org/people/original_sound/sounds/376818/
8)	Main Game Music #1 - Overworld Theme (remaxim) - https://opengameart.org/content/overworld-theme
9)	Fireplace - Fire Burning Loop (midimagician) - https://freesound.org/people/midimagician/sounds/249418/
10)	Water Splach – Splash (swordofkings128) - https://freesound.org/people/swordofkings128/sounds/398032/
11)	Fear Music - a dark and desperate intro (anonymous fix) - https://opengameart.org/content/dark-and-desperate-intro
12)	Music when you solved a problem - Win Music #2 (remaxim) - https://opengameart.org/content/win-music-2
13)	Music for good ending - The King's Crowning (Telaron) - https://opengameart.org/content/the-kings-crowning
14)	Main Game Music #2 - Overworld mockup (remaxim) - https://opengameart.org/content/overworld-mockup
15)	Sheep scream -  Sheep_Bleat (n_audioman) - https://freesound.org/people/n_audioman/sounds/321967/
16)	Fire stealing - Fire Spell 01 (DiscoveryME) - https://freesound.org/people/DiscoveryME/sounds/275608/
17)	Alternative fire stealing - FireSpell1 (alonsotm) - https://freesound.org/people/alonsotm/sounds/396501/
18)	Alternative fire stealing #2 - SFX_MAGIC_FIREBALL_001 (JoelAudio) - https://freesound.org/people/JoelAudio/sounds/77691/
19)	Dragon fire - Dragon Spit Fire 1 (OGsoundFX) - https://freesound.org/people/OGsoundFX/sounds/423009/
20)	Dragon fly - Waving Torch (spookymodem) - https://freesound.org/people/spookymodem/sounds/249809/
21)	Drop item (universal) – drop06 (newagesoup) - https://freesound.org/people/newagesoup/sounds/339364/
22)	Pick up item – SFX Player Action: Phone Pick Up (trullilulli) - https://freesound.org/people/trullilulli/sounds/422651/
23)	Alternative pick up item -  Phone Pickup (ThunderQuads) - https://freesound.org/people/ThunderQuads/sounds/467199/
24)	Knock of wood – 23-Golpe contra madera-consolidated (JVoltio1127) - https://freesound.org/people/JVoltio1127/sounds/406850/
25)	Build wood wall -  roofhammering02 (WIM) - https://freesound.org/people/WIM/sounds/25061/
26)	Main Game Music #3 - Medieval Introduction (Tristan_Lohengrin) - https://freesound.org/people/Tristan_Lohengrin/sounds/319781/
27)	Main Game Music #4 - Soft Mysterious Harp Loop (VWolfdog) - https://opengameart.org/content/soft-mysterious-harp-loop
28)	Main Game Music #5 - Puzzle tune 1 (rezoner) - https://opengameart.org/content/puzzle-tune-1
29)	Hey - 17-Saludo (LukasEG98) - https://freesound.org/people/LukasEG98/sounds/430227/
30)	Whoosh fire - whoosh fire (sfx4animation) - https://freesound.org/people/sfx4animation/sounds/273663/
31)	Wolf die - 20100912.21h.30m.werewolf (dobroide) - https://freesound.org/people/dobroide/sounds/104653/
32)	Wolf sound – wolf-growl (newagesoup) - https://freesound.org/people/newagesoup/sounds/338674/
33)	Camp sound – fasil-music-1 (xserra) - https://freesound.org/people/xserra/sounds/115699/
34)	City sound – Medieval City – SAMPLE (OGsoundFX) - https://freesound.org/people/OGsoundFX/sounds/423119/
35)	Forest sound – birds 01 (Anthousai) - https://freesound.org/people/Anthousai/sounds/398736/
36)	River sound – River_rapid_Camp_Blommaberg_3 (alexkandrell) - https://freesound.org/people/alexkandrell/sounds/320031/
37)	Dragon Roar - Dino Hiss Dragon Roar (syrvive) - https://freesound.org/people/syrvive/sounds/320345/
38)	Meat eating - rip_tear FLESH!!!! (aust_paul) - https://freesound.org/people/aust_paul/sounds/30928/
39)	Sword clash - Cut Through Armor / Slice / Clang (SypherZent) - https://freesound.org/people/SypherZent/sounds/420675/
40)	Stone clash – Brick Debris Fall 06 (debsound) - https://freesound.org/people/debsound/sounds/437602/
41)	Drinking - Drinking and swallow (dersuperanton) - https://freesound.org/people/dersuperanton/sounds/433645/
42)	Wool – Cotton Candy (Coolshows101sound) - https://freesound.org/people/Coolshows101sound/sounds/255158/
43)	Egg – Cracking Egg (MTJohnson) - https://freesound.org/people/MTJohnson/sounds/426307/
44)	Wood chop - knife_chop (deleted_user_877451) - https://freesound.org/people/deleted_user_877451/sounds/66113/