﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragNDropHandler : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
{
    [Header("SpeechBubble")]
    public GameObject infoBubblePrefab;
    public GameObject info;

    [Header("Dragging")]
    public GameObject goDraggedObject;
    public Rigidbody2D rbDraggedObject;

    public Vector3 lastMousePos;
    public Vector3 mouseVelocity;

    public bool isWhackAMoleTime;

    public GameObject thisGameObject;

    public void Start()
    {
        thisGameObject = this.gameObject;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            goDraggedObject = eventData.pointerDrag;
            goDraggedObject.GetComponent<Collider2D>().enabled = false;
            SpriteRenderer sp;
            if(goDraggedObject.TryGetComponent<SpriteRenderer>(out sp))
                sp.sortingLayerName = "DraggedObject";


            rbDraggedObject = eventData.pointerDrag.GetComponent<Rigidbody2D>();
            rbDraggedObject.constraints = RigidbodyConstraints2D.FreezeRotation;

            lastMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        CursorManager.instance.isPickUp = true;

        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if (rbDraggedObject == null)
                return;

            if (thisGameObject.tag == "Fire")
            {
                if (!AudioManager.instance.isPlaying())
                {
                    AudioManager.PlaySound(AudioManager.GetClips()[8]);
                }
            }

            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            rbDraggedObject.MovePosition(Vector2.Lerp(rbDraggedObject.position, pos, 0.3f));

            mouseVelocity = new Vector2(pos.x - lastMousePos.x, pos.y - lastMousePos.y);
            lastMousePos = pos;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            //message
            //message = GetMessage(eventData.GetType, type);
            if (eventData.pointerEnter != null)
            {
                boxMsg msg; msg.text = ""; msg.title = "";
                ObjectType from, to;
                if (eventData.pointerDrag.TryGetComponent<ObjectType>(out from) && eventData.pointerEnter.TryGetComponent<ObjectType>(out to))
                    msg = GameObject.Find("Background").GetComponent<GameManager>().interaction(from.item, to.item);

                string messageTitle = msg.title;
                string messageText = msg.text;

                if (msg.title != "")
                {
                    Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    info = Instantiate(infoBubblePrefab, pos, Quaternion.identity);
                    info.transform.GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(messageTitle);
                    info.transform.GetChild(0).GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().SetText(messageText);
                    info.GetComponent<Canvas>().worldCamera = Camera.main;
                }
            }
            //draging
            SpriteRenderer sp;
            if (goDraggedObject.TryGetComponent<SpriteRenderer>(out sp))
                sp.sortingLayerName = "Default";
            goDraggedObject.GetComponent<Collider2D>().enabled = true;
            goDraggedObject = null;

            if (!isWhackAMoleTime)
                rbDraggedObject.constraints = RigidbodyConstraints2D.FreezeAll;
            else
                rbDraggedObject.velocity = mouseVelocity * 10;
            rbDraggedObject = null;
        }

        CursorManager.instance.isPickUp = false;
        AudioManager.PlaySound(AudioManager.GetClips()[3]);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            string message = "Oh, oh, oh.... ";

            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            info = Instantiate(infoBubblePrefab, pos, Quaternion.identity);
            info.GetComponentInChildren<TMPro.TextMeshProUGUI>().SetText(message);
            info.GetComponent<Canvas>().worldCamera = Camera.main;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            Destroy(info);
            info = null;
        }
    }
}
