﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ControlManager : MonoBehaviour
{
    [Header("Camera Movement")]
    public float panSpeed;
    public float panDetect;

    //public float zoomSpeed;
    //public float maxZoom;
    //public float minZoom;

    [Space]
    public SpriteRenderer background;
    public Canvas escPopup;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CameraMovement();
    }

    void CameraMovement()
    {
        float camPosX = Camera.main.transform.position.x;
        float camPosY = Camera.main.transform.position.y;
        float camPosZ = Camera.main.transform.position.z;

        float mousePosX = Input.mousePosition.x;
        float mousePosY = Input.mousePosition.y;

        Vector3 cameraBoundLeftDown = Camera.main.ViewportToWorldPoint(new Vector2(0, 0));
        Vector3 cameraBoundTopRight = Camera.main.ViewportToWorldPoint(new Vector2(1, 1));

        if (Input.GetKey(KeyCode.W) || mousePosY < Screen.height && mousePosY > Screen.height - panDetect)
        {
            if (cameraBoundTopRight.y < background.bounds.max.y)
                camPosY += panSpeed;
        }
        else if (Input.GetKey(KeyCode.S) || mousePosY < panDetect && mousePosY > 0)
        {
            if (cameraBoundLeftDown.y > background.bounds.min.y)
                camPosY -= panSpeed;
        }

        if (Input.GetKey(KeyCode.D) || mousePosX < Screen.width && mousePosX > Screen.width - panDetect)
        {
            if (cameraBoundTopRight.x < background.bounds.max.x)
                camPosX += panSpeed;
        }
        else if (Input.GetKey(KeyCode.A) || mousePosX < panDetect && mousePosX > 0)
        {
            if (cameraBoundLeftDown.x > background.bounds.min.x)
                camPosX -= panSpeed;
        }

        Camera.main.transform.position = new Vector3(camPosX, camPosY, camPosZ);

        /*
        //zoom
        if (Input.GetAxis("Mouse ScrollWheel") > 0 && zoom < maxZoom)
            zoom += zoomSpeed;
        else if (Input.GetAxis("Mouse ScrollWheel") < 0 && zoom > minZoom)
            zoom -= zoomSpeed;
        */

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            escPopup.enabled = !escPopup.enabled;
        }
    }

    public void exitToMenu()
    {
        SceneManager.LoadScene(0);
    }

    
}
