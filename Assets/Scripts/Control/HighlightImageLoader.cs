﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighlightImageLoader : MonoBehaviour
{
    // Start is called before the first frame update
    void Update()
    {
        Sprite sprite = gameObject.transform.parent.GetComponentInParent<SpriteRenderer>().sprite;
        gameObject.GetComponent<Image>().sprite = sprite;
    }

}
