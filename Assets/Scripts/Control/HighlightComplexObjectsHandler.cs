﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HighlightComplexObjectsHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject thisGameObject;

    // Start is called before the first frame update
    void Start()
    {
        thisGameObject = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.GetComponent<HighlightableObjectHandler>().OnPointerEnter(eventData);
        }

        if (thisGameObject.tag == "Sheep")
        {
            AudioManager.PlaySound(AudioManager.GetClips()[9]);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.GetComponent<HighlightableObjectHandler>().OnPointerExit(eventData);
        }

        AudioManager.instance.Silent();
    }

}
