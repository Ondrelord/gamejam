﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraggableObjectHandler : MonoBehaviour
{
    public Canvas highlightCanvas;
    // Start is called before the first frame update
    void Start()
    {
        highlightCanvas = GetComponentInChildren<Canvas>();
        highlightCanvas.worldCamera = Camera.main;
        highlightCanvas.enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Highlight()
    {
        if (!highlightCanvas.enabled)
            highlightCanvas.enabled = true;
    }

    public void StopHighlight()
    {
        if (highlightCanvas.enabled)
            highlightCanvas.enabled = false;
    }
}
