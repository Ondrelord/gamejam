﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HighlightableObjectHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public Canvas highlightCanvas;
    public GameObject thisGameObject;
    // Start is called before the first frame update
    void Start()
    {
        if(highlightCanvas == null)
            highlightCanvas = GetComponentInChildren<Canvas>();
        highlightCanvas.worldCamera = Camera.main;
        highlightCanvas.enabled = false;

        thisGameObject = this.gameObject;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!highlightCanvas.enabled)
            highlightCanvas.enabled = true;

        if (thisGameObject.tag == "Fire")
        {
            AudioManager.instance.ActivateLoop();
            AudioManager.PlaySound(AudioManager.GetClips()[7]);
        }
        if (thisGameObject.tag == "Wolf")
        {
            AudioManager.PlaySound(AudioManager.GetClips()[10]);
        }
        if (thisGameObject.tag == "Camp")
        {
            AudioManager.instance.SetVolume(0.3f);
            AudioManager.PlaySound(AudioManager.GetClips()[12]);
        }
        if (thisGameObject.tag == "Tree")
        {
            AudioManager.PlaySound(AudioManager.GetClips()[13]);
        }
        if (thisGameObject.tag == "Dragon")
        {
            AudioManager.PlaySound(AudioManager.GetClips()[14]);
        }
        if (thisGameObject.tag == "Water")
        {
            AudioManager.instance.SetVolume(0.1f);
            AudioManager.PlaySound(AudioManager.GetClips()[15]);
        }
        if (thisGameObject.tag == "Fish")
        {
            AudioManager.instance.SetVolume(0.3f);
            AudioManager.PlaySound(AudioManager.GetClips()[16]);
        }
        if (thisGameObject.tag == "Egg")
        {
            AudioManager.PlaySound(AudioManager.GetClips()[17]);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (highlightCanvas.enabled)
            highlightCanvas.enabled = false;

        AudioManager.instance.SetVolume(0.5f);
        AudioManager.instance.DeactivateLoop();
        AudioManager.instance.Silent();
    }
}
