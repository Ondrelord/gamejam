﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ItemManager
{
    Items availibleItems;
    Items permenanentlyLostItems;
    Items allItems;

    public ItemManager()
    {
        availibleItems = new Items();
        permenanentlyLostItems = new Items();
        permenanentlyLostItems.clear();
        allItems = new Items();
        allItems.addall();
    }

    public bool addAvailaibleItem(Item item)
    {
        return availibleItems.addItem(item);
    }

    public void displayAll()
    {
        availibleItems.addall();
    }

    public bool useItem(Item item)
    {
        if (availibleItems.isIn(item))
        {
            return true;
        }
        return false;
    }

    public bool removeItem(Item item)
    {
        if (availibleItems.isIn(item))
        {
            availibleItems.removeItem(item);
            return true;
        }
        return false;
    }

    public List<Item> geActualtItemsInGame()
    {
        return availibleItems.getActualItemList();
    }

    
}