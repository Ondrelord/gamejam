﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections;
using UnityEngine;


public enum Item
{
    //base stuff
    Axe = 0,
    Shovel,
    PickAxe,
    Fork,
    Spoon,
    Knife,
    Stone,
    Wood,
    Water,
    Fruit,

    //enviroment
    Sheep,
    Tree,

    //crafted stuff?
    Fire,
    Fish,
    Meat,
    Palisade,
    Village1,
    Wool,

    //evil stuff
    DragonEgg,
    Dragon,
    Barbarians,
    Wolf,
    Village2,
    Village3,
    Elder,
    River
};

public class Items
{
    List<Item> items;

    public Items()
    {
        items = new List<Item>();
        items.Add(Item.Elder);
        items.Add(Item.Axe);
        items.Add(Item.Fork);
        items.Add(Item.Knife);
        items.Add(Item.Shovel);
        items.Add(Item.PickAxe);
        items.Add(Item.Spoon);
        items.Add(Item.Fire);
        items.Add(Item.Stone);
        items.Add(Item.Tree);
        items.Add(Item.Barbarians);
        items.Add(Item.Sheep);
        items.Add(Item.Village1);
        items.Add(Item.Water);
    }

    ~Items()
    {
        items.Clear();
    }

    public bool addItem(Item item)
    {
        if (!items.Contains(item))
        {
            items.Add(item);
            return true;
        }
        else
            return false;
    }

    public bool removeItem(Item item)
    {
        if (items.Contains(item))
        {
            items.Remove(item);
            return true;
        }
        else
            return false;
    }

    public bool isIn(Item item)
    {
        return items.Contains(item);
    }

    public void clear()
    {
        items.Clear();
    }

    public List<Item> getActualItemList()
    {
        return items;
    }

    public void addall()
    {
        items.Clear();
        items.Add(Item.Elder);
        items.Add(Item.Axe);
        items.Add(Item.Fork);
        items.Add(Item.Knife);
        items.Add(Item.Shovel);
        items.Add(Item.Spoon);
        items.Add(Item.Fire);
        items.Add(Item.Stone);
        items.Add(Item.Tree);
        items.Add(Item.Barbarians);
        items.Add(Item.Sheep);
        items.Add(Item.Dragon);
        items.Add(Item.DragonEgg);
        items.Add(Item.Fish);
        items.Add(Item.Fruit);
        items.Add(Item.Meat);
        items.Add(Item.Palisade);
        items.Add(Item.PickAxe);
        items.Add(Item.Village1);
        items.Add(Item.Village2);
        items.Add(Item.Village3);
        items.Add(Item.Water);
        items.Add(Item.Wolf);
        items.Add(Item.Wood);
        items.Add(Item.Wool);
        items.Add(Item.Elder);
    }
}
