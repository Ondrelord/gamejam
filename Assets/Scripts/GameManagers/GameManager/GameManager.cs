﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections;
using UnityEngine;

public struct boxMsg
{
    public string title;
    public string text;
}

public class winStats
{
    public bool dragon;
    public bool wolf;
    public bool tree;
    public bool sheep;
    public bool barbarians;
    public bool fishing;

    public bool Water;
    public bool Food;
    public bool Cold;
    public bool Security;
}

public class GameManager : MonoBehaviour
{
    public static int timeToGest = 5;

    public static void debugPrint(string s)
    {
        print(s);
    }

    winStats winstat = new winStats();

    public ProblemManager pManager;
    public EventManager eManager;
    public ItemManager iManager;

    public List<GameObject> objects;

    float time;

    bool stop;

    void Start()
    {
        pManager = new ProblemManager();
        eManager = new EventManager();
        iManager = new ItemManager();

        //winstat = new winStats();

        time = timeToGest;
        stop = false;
        //iManager.displayAll();

        setupGameObjects(iManager.geActualtItemsInGame());

        pManager.activateProblem(ProblemType.Water, iManager);
    }

    void Update()
    {
        if(!stop)
        {
            time -= Time.deltaTime;
            if (pManager.getNumberOfactivatedProblems() < 5 && time <= 0)
            {
                pManager.activateRandomProblem(iManager);
                time = timeToGest;
            }

            if (pManager.getNumberOfactivatedProblems() >= 5)
                stop = true;
        }

        if( iManager != null)
            setupGameObjects(iManager.geActualtItemsInGame());
    }

    public void setupGameObjects(List<Item> items)
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            Transform parent = transform.GetChild(i);
            for (int j = 0; j < parent.childCount; j++)
            {
                GameObject child = parent.GetChild(j).gameObject;
                objects.Add(child);

                //print(child.name);
                ObjectType type;
                if (child.TryGetComponent<ObjectType>(out type))
                {
                    if (!items.Contains(type.item))
                        child.SetActive(false);
                    else
                        child.SetActive(true);
                }
            }
        }
    }

    public boxMsg interaction(Item from, Item to)
    {
        //print(from);
       // print(to);
        boxMsg msg;
        msg.text = "";
        msg.title = "";
        switch (to)
        {
            case Item.Barbarians:
            case Item.Dragon:
            case Item.Sheep:
            case Item.Tree:
            case Item.Wolf:
            case Item.River:
                msg = eManager.Interaction(from, to, iManager, pManager);
                break;
            case Item.Village1:
                pManager.ActionOnProblem(from, iManager);
                break;
            default:
                break;
        }
        setupGameObjects(iManager.geActualtItemsInGame());

        return msg;
    }

    public winStats getWinStats()
    {
        winstat.barbarians = eManager.getBarbariansWin();
            winstat.dragon = eManager.getDragonWin();
            winstat.fishing = eManager.getFishingWin();
            winstat.sheep = eManager.getSheepWin();
            winstat.tree = eManager.getTreeWin();
            winstat.wolf = eManager.getWolfWin();


            winstat.Water = pManager.getWinningWater();
            winstat.Security = pManager.getWinningSecurity();
            winstat.Food = pManager.getWinningFood();
            winstat.Cold = pManager.getWinningCold();

        return winstat;
    }

}


