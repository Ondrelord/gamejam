﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class ProblemManager
{
    IProblem pCold;
    IProblem pFood;
    IProblem pWater;
    IProblem pDragon;
    IProblem pSecurity;
    List<IProblem> allProblems;
    List<IProblem> activeProblems;
    int problemCyc = 0;

    public ProblemManager()
    {
        pCold = new ProblemCold();
        pFood = new ProblemFood();
        pWater = new ProblemWater();
        pDragon = new ProblemDragon();
        pSecurity = new ProblemSecurity();
        allProblems = new List<IProblem>();
        activeProblems = new List<IProblem>();
        allProblems.Add(pCold);
        allProblems.Add(pFood);
        allProblems.Add(pDragon);
        allProblems.Add(pWater);
        allProblems.Add(pSecurity);
    }

    public void ActionOnProblem(Item i, ItemManager iManager)
    {
        foreach(var problem in activeProblems)
        {
            problem.ItemAction(i, iManager);
            problem.tryCompleteQuest();
        }
    }

    public bool activateProblem(ProblemType pt, ItemManager iManager)
    {
        bool isIn = false;
        if(activeProblems.Count >=1)
            foreach(var prob in activeProblems)
            {
                if(prob.getType() == pt)
                {
                    isIn = true;
                }
            }

        if(!isIn)
        {
            switch (pt)
            {
                case ProblemType.Cold:
                    //GameManager.debugPrint("tu nemam byt cold prob");
                    activeProblems.Add(pCold);
                    break;
                case ProblemType.Dragon:
                    //GameManager.debugPrint("tu nemam byt dragon prob");
                    activeProblems.Add(pDragon);
                    iManager.addAvailaibleItem(Item.Dragon);
                    break;
                case ProblemType.Food:
                    //GameManager.debugPrint("tu nemam byt food prob");
                    activeProblems.Add(pFood);
                    break;
                case ProblemType.Security:
                    //GameManager.debugPrint("tu nemam byt security prob");
                    activeProblems.Add(pSecurity);
                    break;
                case ProblemType.Water:
                   // GameManager.debugPrint("tu mam byt water prob");
                       activeProblems.Add(pWater);
                    break;
                default:
                    //uknown problem
                    break;
            }
        }
        return !isIn;
    }

    public void activateRandomProblem(ItemManager iManager)
    {
        if (activeProblems.Count == 4)
        {
            activateProblem(ProblemType.Cold, iManager);
            activateProblem(ProblemType.Dragon, iManager);
            activateProblem(ProblemType.Food, iManager);
            activateProblem(ProblemType.Security, iManager);
            activateProblem(ProblemType.Water, iManager);
            return;
        }

        Random rnd = new Random();
        int prob = rnd.Next(5);
        bool activated = false;
        do
        {
            prob = rnd.Next(0, 5);
            switch (prob)
            {
                case 0:
                    activated = activateProblem(ProblemType.Cold, iManager);
                    break;
                case 1:
                    activated = activateProblem(ProblemType.Dragon, iManager);
                    break;
                case 2:
                    activated = activateProblem(ProblemType.Food, iManager);
                    break;
                case 3:
                    activated = activateProblem(ProblemType.Security, iManager);
                    break;
                case 4:
                    activated = activateProblem(ProblemType.Water, iManager);
                    break;
                default:
                    //uknown problem
                    break;
            }
        } while (!activated);
    }

    public int getNumberOfactivatedProblems()
    {
        return activeProblems.Count;
    }

    public string getActiveProblemsString()
    {
        string probStr = activeProblems[problemCyc++].getDescriptionString();
        if (problemCyc >= activeProblems.Count) problemCyc = 0;

        return probStr;
    }

    public bool getWinningCold()
    {
        return pCold.solve();
    }

    public bool getWinningFood()
    {
        return pFood.solve();
    }

    public bool getWinningSecurity()
    {
        return pSecurity.solve();
    }

    public bool getWinningWater()
    {
        return pWater.solve();
    }
}
