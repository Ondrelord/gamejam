﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//defines what problem modifications we could have
public enum problemState
{
    smthg = 0
}

//defines problem types for visual objects
public enum ProblemType
{
    Water = 0,
    Food,
    Cold,
    Security,
    Dragon
}

//problem interface (used when need to store which problem are affected by one problem)
public interface IProblem
{
    void ItemAction(Item item, ItemManager iManager);
    void resetQuest();
    bool tryCompleteQuest();
    void itemLost(Item item);
    string getDescriptionString();
    ProblemType getType();
    bool solve();
}

//heredity of common params
public abstract class Problem
{
    protected bool isSolved;
    protected int score;
    public ProblemType pType;
}

//main class for problems
public class ProblemCold : Problem, IProblem
{
    bool flamableThing;
    bool stone;

    //constructor
    public ProblemCold()
    {
        isSolved = false;
        score = 0;
        flamableThing = false;
        stone = false;
        pType = ProblemType.Cold;
    }

    public void  resetQuest()
    {
        isSolved = false;
        score = 0;
        flamableThing = false;
        stone = false;
    }

    public ProblemType getType()
    {
        return pType;
    }

    //defines what happens, when you use any item for solving this problem
    public void ItemAction(Item item, ItemManager iManager)
    {
        switch (item)
        {
            //right decision
            case Item.Stone:
                stone = true;
                iManager.removeItem(Item.Wood);
                break;
            case Item.Wood:
                flamableThing = true;
                break;
            case Item.Fire:
                isSolved = true;
                break;

            //no effekt
            default:
                break;
        }
    }

    //called after wrong decision done
    void wrong(string s)
    {
        //Console.Write(s);
        score--;
    }

    public void itemLost(Item item)
    {
        if(item == Item.Fire)
        {
            isSolved = false;
        }
    }

    //after using item, try to complete quest. Otherwise it could by called anytime 
    public bool tryCompleteQuest()
    {
        if (isSolved) return true;

        if (flamableThing && stone)
        {
            isSolved = true;
            return true;
        }
        else
            return false;
    }

    public string getDescriptionString()
    {
        return "Winter is coming, can you help us prepare for the cold?";
    }

    public bool solve()
    {
        return isSolved;
    }
}

public class ProblemSecurity : Problem, IProblem
{
    bool palisade;
    bool barbariansFriend;
    bool Fire;

    //constructor
    public ProblemSecurity()
    {
        isSolved = false;
        score = 0;
        palisade = false;
        barbariansFriend = false;
        Fire = false;
        pType = ProblemType.Security;
    }

    public void resetQuest()
    {
        isSolved = false;
        score = 0;
        palisade = false;
        barbariansFriend = false;
        Fire = false;
    }

    public ProblemType getType()
    {
        return pType;
    }

    //defines what happens, when you use any item for solving this problem
    public void ItemAction(Item item ,ItemManager iManager)
    {
        switch (item)
        {
            case Item.Wood:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[7]);
                palisade = true;
                iManager.addAvailaibleItem(Item.Palisade);
                iManager.removeItem(Item.Wood);
                //TODO: create palisade
                break;
            case Item.Fire:
                Fire = true;
                iManager.addAvailaibleItem(Item.Fire);
                break;
            case Item.Barbarians:
                barbariansFriend = true;
                break;
            //no effekt
            default:
                //shod be caled to get text from same msg container
                //wrong("this will not help you :D", item);
                break;
        }
    }

    //called after wrong decision done
    void wrong(string s, Item item)
    {
        //Console.Write(s);
        score--;
    }

    public void itemLost(Item item)
    {
        switch (item)
        {
            case Item.Palisade:
                palisade = false;
                //TODO: create palisade
                break;
            case Item.Fire:
                Fire = false;
                break;
            case Item.Barbarians:
                barbariansFriend = false;
                break;
            //no effekt
            default:
                //shod be caled to get text from same msg container
                //wrong("this will not help you :D", item);
                break;
        }
    }

    //after using item, try to complete quest. Otherwise it could by called anytime 
    public bool tryCompleteQuest()
    {
        if (isSolved) return true;

        int tmp = 0;
        if (palisade) tmp++;
        if (barbariansFriend) tmp++;
        if (Fire) tmp++;

        if (tmp > 1)
        {
            isSolved = true;
            return true;
        }
        else
            return false;
    }

    public void summonBarbarians()
    {
        barbariansFriend = true;
        tryCompleteQuest();
        //TODO: send masage to replace camp
    }

    public string getDescriptionString()
    {
        return "We have a nice village here, but Danger creeps everywhere. Please protect us.";
    }

    public bool solve()
    {
        return isSolved;
    }
}

public class ProblemWater : Problem, IProblem
{
    bool water;
    public ProblemWater()
    {
        isSolved = false;
        score = 0;
        water = false;
        pType = ProblemType.Water;
    }

    public void resetQuest()
    {
        isSolved = false;
        score = 0;
        water = false;
    }

    public ProblemType getType()
    {
        return pType;
    }

    //defines what happens, when you use any item for solving this problem
    public void ItemAction(Item item, ItemManager iManager)
    {
        switch (item)
        {
            case Item.Water:
                water = true;
                break;
            //no effekt
            default:
                //shod be caled to get text from same msg container
                //wrong("this will not help you :D", item);
                break;
        }
    }

    public void wrong(string s)
    {
        //Console.Write(s);
        score--;
    }

    public void itemLost(Item item)
    {
        if(item == Item.Water)
        {
            isSolved = false;
        }
    }

    public bool tryCompleteQuest()
    {
        if (isSolved) return true;

        if(water)
        {
            isSolved = true;
            score++;
            return true;
        }
        return false;
    }

    public string getDescriptionString()
    {
        return "This season is dry, people are thirsty. Can you help us?";
    }

    public bool solve()
    {
        return isSolved;
    }

}

public class ProblemFood: Problem, IProblem
{
    bool fruit;
    bool meat;
    bool fish;

    public ProblemFood()
    {
        isSolved = false;
        score = 0;
        fruit = false;
        meat = false;
        fish = false;
        pType = ProblemType.Food;
    }

    public void resetQuest()
    {
        isSolved = false;
        score = 0;
        fruit = false;
        meat = false;
        fish = false;
    }

    public ProblemType getType()
    {
        return pType;
    }

    //defines what happens, when you use any item for solving this problem
    public void ItemAction(Item item, ItemManager iManager)
    {
        switch (item)
        {
            case Item.Fruit:
                fruit = true;
                if(!isSolved)
                    iManager.removeItem(Item.Fruit);
                break;
            case Item.Meat:
                meat = true;
                if (!isSolved)
                    iManager.removeItem(Item.Meat);
                break;
            case Item.Fish:
                fish = true;
                if (!isSolved)
                    iManager.removeItem(Item.Fish);
                break;
            //no effekt
            default:
                //shod be caled to get text from same msg container
                //wrong("this will not help you :D", item);
                break;
        }
    }

    public void itemLost(Item item)
    {
        switch (item)
        {
            case Item.Fruit:
                fruit = false;
                break;
            case Item.Meat:
                meat = false;
                break;
            case Item.Fish:
                fish = false;
                break;
            //no effekt
            default:
                //shod be caled to get text from same msg container
                //wrong("this will not help you :D", item);
                break;
        }
    }

    public void wrong(string s)
    {
        //Console.Write(s);
        score--;
    }

    public bool tryCompleteQuest()
    {
        if (isSolved) return true;

        if (fruit || meat || fish)
        {
            isSolved = true;
            score++;
            return true;
        }
        return false;
    }

    public string getDescriptionString()
    {
        return "Aren't you hungry, because I'm always hangry. Can you get us somthing to eat?";
    }

    public bool solve()
    {
        return isSolved;
    }
}

public class ProblemDragon : Problem, IProblem
{
    bool dragonegg;

    public ProblemDragon()
    {
        isSolved = false;
        score = 0;
        dragonegg = false;
        pType = ProblemType.Dragon;
    }

    public void resetQuest()
    {
        isSolved = false;
        score = 0;
        dragonegg = false;
    }

    public ProblemType getType()
    {
        return pType;
    }

    //defines what happens, when you use any item for solving this problem
    public void ItemAction(Item item, ItemManager iManager)
    {
        switch (item)
        {
            case Item.DragonEgg:
                dragonegg = true;
                break;
            //no effekt
            default:
                //shod be caled to get text from same msg container
                //wrong("this will not help you :D", item);
                break;
        }
    }

    public void wrong(string s)
    {
        //Console.Write(s);
        score--;
    }

    public void itemLost(Item item)
    {
        if (item == Item.DragonEgg)
            dragonegg = false;
    }

    public bool tryCompleteQuest()
    {
        if (isSolved) return true;

        if (dragonegg)
        {
            score++;
            isSolved = true;
            return true;
        }
        else
            return false;
    }

    public string getDescriptionString()
    {
        return "Ou no, a Dragon in our quite town? This will be a desolation.";
    }



    public bool solve()
    {
        return isSolved;
    }
}