﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class EventManager
{
    //IEvent bear;
    IEvent dragon;
    IEvent wolf;
    IEvent tree;
    IEvent sheep;
    IEvent barbarians;
    IEvent fishing;
    bool eggIsAvailable;

    public EventManager()
    {
       // bear = new EventBear();
        dragon = new EventDragon();
        wolf = new EventWolf();
        tree = new EventTree();
        sheep = new EventSheep();
        barbarians = new EventBarbarians();
        fishing = new EventFishing();
        eggIsAvailable = false; ;
    }

    public boxMsg Interaction(Item from, Item to, ItemManager iManager, ProblemManager pManager)
    {
        boxMsg outMsg;

        if(from == Item.Stone && !eggIsAvailable)
        {
            eggIsAvailable = true;
            iManager.addAvailaibleItem(Item.DragonEgg);
        }

        switch(to)
        {
            case Item.Barbarians:
                outMsg = barbarians.ItemAction(from, iManager, pManager);
                break;
            case Item.Dragon:
                outMsg = dragon.ItemAction(from, iManager, pManager);
                break;
            case Item.Sheep:
                outMsg = sheep.ItemAction(from, iManager, pManager);
                break;
            case Item.Tree:
                outMsg = tree.ItemAction(from, iManager, pManager);
                break;
            case Item.Wolf:
                outMsg = wolf.ItemAction(from, iManager, pManager);
                break;
            case Item.River:
                outMsg = wolf.ItemAction(from, iManager, pManager);
                break;

            /*case Item.Village1:
                outMsg = wolf.ItemAction(from, iManager, pManager);*/
            default:
                outMsg.title = "Uknown event.";
                outMsg.text = "";
                break;
        }

        return outMsg;
    }

    public bool getDragonWin()
    {
        return dragon.solved();
    }

    public bool getSheepWin()
    {
        return sheep.solved();
    }

    public bool getTreeWin()
    {
        return tree.solved();
    }

    public bool getWolfWin()
    {
        return wolf.solved();
    }

    public bool getBarbariansWin()
    {
        return barbarians.solved();
    }

    public bool getFishingWin()
    {
        return fishing.solved();
    }
}