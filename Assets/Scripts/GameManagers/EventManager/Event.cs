﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections;
using UnityEngine;

public interface IEvent
{
    boxMsg ItemAction(Item item, ItemManager iManager, ProblemManager pManager);

    bool solved();

}

public abstract class Event
{
    protected bool isSolved;
    protected int score;

    public static boxMsg errMsg()
    {
        boxMsg outMsg;
        outMsg.title = "Item inaccessible.";
        outMsg.text = "";
        return outMsg;
    }
}

public class EventDragon : Event, IEvent
{
    bool isAngry;

    public EventDragon()
    {
        isSolved = false;
        isAngry = false;
        score = 0;
    }

    public boxMsg ItemAction(Item item, ItemManager iManager, ProblemManager pManager)
    {
        boxMsg outMsg;

        if (!iManager.useItem(item))
        {
            outMsg = errMsg();
            return outMsg;
        }

        switch (item)
        {
            case Item.DragonEgg:
                outMsg.title = "Good job.";
                outMsg.text = "Dragon lets you go. We can only hope it will not come back.";
                iManager.removeItem(item);
                isSolved = true;
                break;
            default:
                outMsg.title = "Item lost.";
                outMsg.text = "Dragon is selfish creature.";
                iManager.removeItem(item);
                break;
        }
        return outMsg;
    }
    public bool solved()
    {
        return isSolved;
    }

}

public class EventTree : Event, IEvent
{
    public EventTree()
    {
        isSolved = false;
        score = 0;
    }

    string getChopDownText()
    {
        System.Random rnd = new System.Random();
        int comment = rnd.Next(4);
        switch (comment)
        {
            case 0:
                return "Have you even finished elementary school?";
            case 1:
                return "Your mum must be proud of you...";
            case 2:
                return "Your father is saying, that he has alibi for the night you was begotten, right?";
            case 3:
                return "Do you think we have whole life to chop down a tree?";
            default:
                return "";
        }
    }

    public boxMsg ItemAction(Item item, ItemManager iManager, ProblemManager pManager)
    {
        boxMsg outMsg;

        if (!iManager.useItem(item))
        {
            outMsg = errMsg();
            return outMsg;
        }

        switch (item)
        {
            //ok
            case Item.Axe:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[2]);
                outMsg.title = "Good job.";
                outMsg.text = "You know, what to do with the axe.";
                isSolved = true;
                iManager.removeItem(Item.Tree);
                iManager.addAvailaibleItem(Item.Wood);
                //Console.Write("Axe");
                score++;
                break;

            //loosing stuff, fun msg
            case Item.Fork:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[2]);
                outMsg.title = "Collecting food.";
                outMsg.text = "Nice. You collected some food";
                //iManager.removeItem(Item.Tree);
                //iManager.removeItem(Item.Fork);
                iManager.addAvailaibleItem(Item.Fruit);
                pManager.ActionOnProblem(Item.Fruit, iManager);
                //Console.Write("Fork");
                break;
            case Item.Knife:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[2]);
                outMsg.title = "Choping down a tree with knife.";
                outMsg.text = getChopDownText();
                iManager.removeItem(Item.Tree);
                iManager.removeItem(Item.Fork);
                iManager.addAvailaibleItem(Item.Wood);
                //Console.Write("Knife");
                break;
            case Item.PickAxe:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[2]);
                outMsg.title = "Choping down a tree with pickaxe.";
                outMsg.text = getChopDownText();
                iManager.removeItem(Item.Tree);
                iManager.removeItem(Item.Fork);
                iManager.addAvailaibleItem(Item.Wood);
                //Console.Write("PickAxe");
                break;
            case Item.Spoon:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[2]);
                outMsg.title = "Choping down a tree with spoon.";
                outMsg.text = getChopDownText();
                iManager.removeItem(Item.Tree);
                iManager.removeItem(Item.Fork);
                iManager.addAvailaibleItem(Item.Wood);
                ////Console.Write("Spoon");
                break;

            //feeding bear
            case Item.Fish:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[1]);
                outMsg.title = "Bear feeding.";
                outMsg.text = "You are really kind, but bears don't say \"Thank you\".";
                iManager.removeItem(Item.Fish);
                ////Console.Write("Fish");
                break;
            case Item.Meat:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[1]);
                outMsg.title = "Bear feeding.";
                outMsg.text = "You are really kind, but bears don't say \"Thank you\".";
                iManager.removeItem(Item.Meat);
                ////Console.Write("Meat");
                break;

            case Item.Stone:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[3]);
                outMsg.title = "Item lost.";
                outMsg.text = "I hope, the rock is on the better place now.";
                ////Console.Write("Stone");
                iManager.removeItem(item);
                break;
            default:
                ////Console.Write("Not tested part!!!");
                outMsg.title = "Item lost.";
                outMsg.text = "";
                break;
        }
        return outMsg;
    }

    public bool solved()
    {
        return isSolved;
    }
}

public class EventBarbarians : Event, IEvent
{
    bool isAngry;
    bool fire;

    public EventBarbarians()
    {
        isSolved = false;
        isAngry = false;
        score = 0;
        fire = true;
    }

    public boxMsg ItemAction(Item item, ItemManager iManager, ProblemManager pManager)
    {
        boxMsg outMsg;

        if (!iManager.useItem(item))
        {
            outMsg = errMsg();
            return outMsg;
        }

        switch (item)
        {
            //kill
            case Item.Axe:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[2]);
                outMsg.title = "Slay them all!";
                outMsg.text = "How could you do that? The USA didn't attack oil wells in Irack with such force.";
                isSolved = true;
                iManager.removeItem(Item.Barbarians);
                iManager.removeItem(Item.Fire);
                //Console.Write("Axe");
                score += 2;
                break;
            case Item.Knife:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[2]);
                outMsg.title = "Slay them all!";
                outMsg.text = "Saddám should learn from you...";
                isSolved = true;
                iManager.removeItem(Item.Barbarians);
                iManager.removeItem(Item.Fire);
                //Console.Write("Knife");
                score += 2;
                //get sadam
                break;

            //loosing stuff, fun msg
            case Item.Fork:
                outMsg.title = "Item lost.";
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[2]);
                outMsg.text = "Really? Did you think it is fun to attack barbarians with fork? ...Your ass is hurting now... Haha... Stupid...";
                iManager.removeItem(Item.Fork);
                //Console.Write("Fork");
                break;
            case Item.Spoon:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[2]);
                outMsg.title = "Item lost.";
                outMsg.text = "Really? Did you think it is fun to attack barbarians with spoon? ...Your ass is hurting now... Haha... Stupid...";
                iManager.removeItem(Item.Spoon);
                //Console.Write("Spoon");
                break;

            //run problem security
            case Item.PickAxe:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[2]);
                outMsg.title = "Item lost.";
                outMsg.text = "Are you trying to destroy their tents? Too bad for you.";
                isAngry = true;
                iManager.removeItem(Item.PickAxe);
                //Console.Write("PickAxe");
                break;
            case Item.Shovel:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[2]);
                outMsg.title = "Item lost.";
                outMsg.text = "Are you trying to destroy their tents? Too bad for you.";
                isAngry = true;
                iManager.removeItem(Item.Shovel);
                //Console.Write("PickAxe");
                break;

            //feeding bear
            case Item.Fish:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[1]);
                outMsg.title = "New friends.";
                outMsg.text = "It looks like you found new friend ...you are no longer alone... I mean in the game, in real life you are still alone.";
                isSolved = true;
                iManager.removeItem(Item.Fish);
                iManager.removeItem(Item.Fire);
                //Console.Write("Fish");
                //msg about bears
                Vector3 cenPos = GameObject.Find("VillageCenter").transform.position;
                if (GameObject.Find("Clearing").transform.GetChild(0).GetComponent<ObjectType>().item == Item.Barbarians)
                    GameObject.Find("Clearing").transform.GetChild(0).transform.position = cenPos;
                else
                    GameObject.Find("Clearing").transform.GetChild(1).transform.position = cenPos;
                break;
            case Item.Meat:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[1]);
                outMsg.title = "New friends.";
                outMsg.text = "It looks like you found new friend ...you are no longer alone... I mean in the game, in real life you are still alone.";
                iManager.removeItem(Item.Meat);
                iManager.removeItem(Item.Fire);
                isSolved = true;
                //Console.Write("Meat");
                Vector3 cenPos1 = GameObject.Find("VillageCenter").transform.position;
                if (GameObject.Find("Clearing").transform.GetChild(0).GetComponent<ObjectType>().item == Item.Barbarians)
                    GameObject.Find("Clearing").transform.GetChild(0).transform.position = cenPos1;
                else
                    GameObject.Find("Clearing").transform.GetChild(1).transform.position = cenPos1;
                //msg about bears
                break;

            //angry
            case Item.Stone:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[3]);
                outMsg.title = "Enemies will come.";
                outMsg.text = "Nice throw. Right to the barbarian leader's head.";
                isAngry = true;
                iManager.removeItem(Item.Stone);
                //Console.Write("Stone");
                break;
            default:
                //Console.Write("Not tested part!!!");
                outMsg.title = "Item lost.";
                outMsg.text = "";
                break;
        }
        return outMsg;
    }

    public bool solved()
    {
        return isSolved;
    }

}

public class EventWolf : Event, IEvent
{
    bool isHungry;

    public EventWolf()
    {
        isSolved = false;
        score = 0;
        isHungry = false;
    }

    public boxMsg ItemAction(Item item, ItemManager iManager, ProblemManager pManager)
    {
        boxMsg outMsg;

        if (!iManager.useItem(item))
        {
            outMsg = errMsg();
            outMsg.text = item.ToString();
            return outMsg;
        }

        switch (item)
        {
            case Item.Axe:
            case Item.Fork:
            case Item.Knife:
            case Item.PickAxe:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[2]);
                outMsg.title = "Fighting with wolf.";
                outMsg.text = "What a bloody show";
                isSolved = true;
                iManager.removeItem(Item.Wolf);
                iManager.removeItem(item);
                iManager.addAvailaibleItem(Item.Meat);
                break;

            case Item.Fish:
            case Item.Meat:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[1]);
                outMsg.title = "Meat thrown to the wolf.";
                outMsg.text = "It is away, for a while.";
                iManager.removeItem(item);
                iManager.removeItem(Item.Wolf);
                isHungry = false;
                break;

            case Item.Fire:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[0]);
                outMsg.title = "Good job.";
                outMsg.text = "Wolfs are scared of fire. It runs away for its life.";
                iManager.removeItem(Item.Wolf);
                isSolved = true;
                break;
            case Item.Fruit:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[1]);
                outMsg.title = "Item lost.";
                outMsg.text = "Obviously, you missed all biology classes.";
                iManager.removeItem(Item.Fruit);
                //Console.Write("Fruit");
                break;

            case Item.Shovel:
            case Item.Spoon:
            case Item.Stone:
            case Item.Wood:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[6]);
                outMsg.title = "Item thrown.";
                outMsg.text = "Of corse that you can throw your stuff after the wolf. You succeed this time";
                iManager.removeItem(item);
                iManager.removeItem(Item.Wolf);
                //Console.Write("Stone");
                break;
            case Item.Water:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[4]);
                outMsg.title = "Doing something useless.";
                outMsg.text = "It looks like it is not thirsty.";
                break;
            case Item.Wool:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[5]);
                outMsg.title = "Item lost.";
                outMsg.text = "The wolf lookes like he is cold? Then why are you doing this?";
                iManager.removeItem(item);
                //Console.Write("Wool");
                break;
            default:
                //dragon....
                //Console.Write("Not tested part!!!");
                outMsg.title = "Item lost.";
                outMsg.text = "";
                break;
        }
        return outMsg;
    }

    public bool solved()
    {
        return isSolved;
    }
}

public class EventSheep : Event, IEvent
{
    public EventSheep()
    {
        isSolved = false;
        score = 0;
    }

    public boxMsg ItemAction(Item item, ItemManager iManager, ProblemManager pManager)
    {
        boxMsg outMsg;

        if (!iManager.useItem(item))
        {
            outMsg = errMsg();
            return outMsg;
        }

        switch (item)
        {
            case Item.Axe:
            case Item.Fork:
            case Item.PickAxe:
            case Item.Shovel:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[2]);
                outMsg.title = "Blood everywhere.";
                outMsg.text = "Nice bloody show";
                isSolved = true;
                iManager.removeItem(Item.Sheep);
                iManager.addAvailaibleItem(Item.Wolf);
                pManager.activateProblem(ProblemType.Security, iManager);
                iManager.addAvailaibleItem(Item.Meat);
                break;

            case Item.Knife:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[2]);
                //Console.Write("Knife");
                outMsg.title = "Good job.";
                outMsg.text = "One cut and all is over.";
                isSolved = true;
                iManager.addAvailaibleItem(Item.Meat);
                iManager.addAvailaibleItem(Item.Wool);
                iManager.removeItem(Item.Sheep);
                pManager.activateProblem(ProblemType.Security, iManager);
                iManager.addAvailaibleItem(Item.Wolf);
                break;

            case Item.Fish:
            case Item.Meat:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[1]);
                //Console.Write("Meat/fish");
                outMsg.title = "Feeding?";
                outMsg.text = "You should read some biology books.";
                isSolved = true;
                iManager.removeItem(item);
                break;

            case Item.Fire:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[0]);
                Console.Write("Fire");
                //Console.Write("Fire");
                outMsg.title = "Fire everywhere.";
                outMsg.text = "Poor sheep. You must hope PETA will never hear about this.";
                isSolved = true;
                iManager.removeItem(Item.Sheep);
                iManager.addAvailaibleItem(Item.Wolf);
                pManager.activateProblem(ProblemType.Security, iManager);
                iManager.addAvailaibleItem(Item.Meat);
                iManager.removeItem(item);

                break;
            case Item.Fruit:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[1]);
                //Console.Write("Fruit");
                outMsg.title = "Sheep feeding.";
                outMsg.text = "It lookes like sheep enjoys it.";
                isSolved = true;
                iManager.removeItem(Item.Fruit);
                break;

            case Item.Spoon:
            case Item.Stone:
            case Item.Wood:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[6]);
                outMsg.title = "Throwing item.";
                outMsg.text = "I am calling PETA. This is unaccepteble.";
                iManager.removeItem(item);
                //Console.Write("Wood");
                break;
            case Item.Water:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[4]);
                //Console.Write("Water");
                outMsg.title = "Sheep drinking.";
                outMsg.text = "It was thirsty.";
                break;
            default:
                //TODO: dragon, wolf...
                ////Console.Write("Not tested part!!!");
                outMsg.title = "Item lost.";
                outMsg.text = "";
                break;

        }
        return outMsg;
    }

    public bool solved()
    {
        return isSolved;
    }
}

public class EventFishing : Event, IEvent
{
    bool wood;
    bool stone;
    bool wool;
    public EventFishing()
    {
        isSolved = false;
        score = 0;
        wood = false;
        wool = false;
    }

    public boxMsg ItemAction(Item item, ItemManager iManager, ProblemManager pManager)
    {
        boxMsg outMsg;

        if (!iManager.useItem(item))
        {
            outMsg = errMsg();
            return outMsg;
        }

        switch (item)
        {
            case Item.Axe:
            case Item.Fork:
            case Item.PickAxe:
            case Item.Shovel:
            case Item.Knife:
            case Item.Meat:
            case Item.Fire:
            case Item.Fruit:
            case Item.Spoon:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[2]);
                outMsg.title = "Item floating away.";
                outMsg.text = "Do you like watching your stuff to float away?";
                iManager.removeItem(item);
                break;
            case Item.Fish:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[1]);
                outMsg.title = "Lets free the fish.";
                outMsg.text = "You set that poor fish free? You are a good man.";
                iManager.removeItem(item);
                break;
            case Item.Stone:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[3]);
                outMsg.title = "Building dam.";
                outMsg.text = "The rock was big enought to build a dam.";
                stone = true;
                break;
            case Item.Wood:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[6]);
                outMsg.title = "Building dam.";
                outMsg.text = "Dam could be useful.";
                wood = true;
                iManager.removeItem(item);
                break;
            case Item.Water:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[4]);
                outMsg.title = "Pouring water.";
                outMsg.text = "Is it good idea?.";
                break;
            case Item.Wool:
                EventAudioManager.PlaySound(EventAudioManager.GetClips()[5]);
                outMsg.title = "Accesing fishing yeld.";
                outMsg.text = "Look at that smart boy, he did it.";
                if (wood || stone)
                {
                    iManager.addAvailaibleItem(Item.Fish);
                    isSolved = true;
                }
                break;
            default:
                //TODO: dragon, wolf...
                ////Console.Write("Not tested part!!!");
                /*outMsg.title = "Item lost.";
                outMsg.text = "Not tested part!!!";*/
                outMsg.title = "";
                outMsg.text = "";
                break;

        }
        return outMsg;
    }

    public bool solved()
    {
        return isSolved;
    }
}
