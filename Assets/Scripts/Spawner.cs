﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject []villagers;

    public float recharge;
    public GameObject zone;

    public bool isWhackAMoleTime;

    // Update is called once per frame
    void Update()
    {
        if (zone.transform.childCount >= 15)
            return;

        if (recharge <= 0)
        {
            Vector2 zoneExtents = zone.GetComponent<SpriteRenderer>().bounds.extents;
            Vector2 zoneCenter = zone.GetComponent<SpriteRenderer>().bounds.center;

            Vector2 rnd = Random.insideUnitCircle;
            Vector2 target = new Vector2(zoneCenter.x + rnd.x * zoneExtents.x, zoneCenter.y + rnd.y * zoneExtents.y);

            GameObject mob = Instantiate(villagers[Random.Range(0,villagers.Length)], target, Quaternion.identity);
            mob.GetComponent<PeasantBehaviour>().zone = zone;
            mob.transform.SetParent(zone.transform);

            recharge = Random.Range(zone.transform.childCount - 2f, zone.transform.childCount);
            recharge = !isWhackAMoleTime ? Mathf.Min(recharge, 4f) : 0.5f;
        }
        else
        {
            recharge -= Time.deltaTime;
        }
    }
}
