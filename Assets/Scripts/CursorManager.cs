﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour
{
    public Texture2D mouseTexture;
    public Texture2D mouseTexture2;
    public static CursorManager instance;
    public bool isPickUp;

    void Start()
    {
        if (instance == null) instance = this;
    }

    private void Update()
    {
        if (!this.isActiveAndEnabled)
        {
            instance = null;
        }
        else
        {
            instance = this;
        }
    }

    void Awake()
    {
        UnityEngine.Cursor.visible = false;
    }

    public void ChangeState()
    {
        if (!instance.isPickUp)
        {
            instance.isPickUp = true;
        }
        else
        {
            instance.isPickUp = false;
        }
    }

    void OnGUI()
    {
        if (!isPickUp)
        {
            Vector2 m = UnityEngine.Event.current.mousePosition;
            GUI.depth = 0;
            GUI.Label(new Rect(m.x - mouseTexture.width/3, m.y - mouseTexture.height/3, mouseTexture.width, mouseTexture.height), mouseTexture);
        }
        else if (isPickUp)
        {
            Vector2 m = UnityEngine.Event.current.mousePosition;
            GUI.depth = 0;
            GUI.Label(new Rect(m.x, m.y, mouseTexture2.width, mouseTexture2.height), mouseTexture2);
        }
    }
}
