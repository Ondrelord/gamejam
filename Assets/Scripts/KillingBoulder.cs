﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillingBoulder : MonoBehaviour
{
    public float dropTime;

    public bool isWhackAMoleDone;
    public bool isWhackAMoleTime;
    public Camera whackAMoleCamera;
    public float timeleft;

    public Spawner spawner;
    public GameObject mainCamera { get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        isWhackAMoleDone = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isWhackAMoleTime)
        {
            if (gameObject.GetComponent<SpriteRenderer>().sortingLayerName == "DraggedObject")
                dropTime = 0.25f;

            if (dropTime > 0)
                dropTime -= Time.deltaTime;
        }
        else
        {
            if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) < 0.1f && Mathf.Abs(GetComponent<Rigidbody2D>().velocity.y) < 0.1f)
                GetComponent<Rigidbody2D>().velocity = Vector2.zero;

            if (timeleft >= 0)
                timeleft -= Time.deltaTime;
            else
                EndWhackAMoleTime();
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        ObjectType type;
        if (collision.gameObject.TryGetComponent<ObjectType>(out type) && isPeasant(type.item))
            if (!isWhackAMoleTime)
            {
                if (dropTime > 0f )
                {
                    Destroy(collision.gameObject);
                    if (!isWhackAMoleDone)
                        ItsWhackAMoleTime();
                }
            }
            else
            {
                if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > 0.1f || Mathf.Abs(GetComponent<Rigidbody2D>().velocity.y) > 0.1f)
                    Destroy(collision.gameObject);
            }

        AudioManager.PlaySound(AudioManager.GetClips()[4]);
    }

    private void ItsWhackAMoleTime()
    {
        isWhackAMoleTime = true;
        GetComponent<DragNDropHandler>().isWhackAMoleTime = true;
        spawner.isWhackAMoleTime = true;

        mainCamera = Camera.main.gameObject;
        mainCamera.SetActive(false);
        whackAMoleCamera.gameObject.SetActive(true);
        timeleft = 15f;
    }

    private void EndWhackAMoleTime()
    {
        isWhackAMoleTime = false;
        GetComponent<DragNDropHandler>().isWhackAMoleTime = false;
        spawner.isWhackAMoleTime = false;

        mainCamera.SetActive(true);
        whackAMoleCamera.gameObject.SetActive(false);

        isWhackAMoleDone = true;
    }

    bool isPeasant(Item item)
    {
        switch(item)
        {
            case Item.Axe:
            case Item.Elder:
            case Item.Fork:
            case Item.Knife:
            case Item.PickAxe:
            case Item.Shovel:
            case Item.Spoon:
                return true;
            default:
                return false;
        }
    }
}
