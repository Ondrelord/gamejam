﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Whackamole : MonoBehaviour
{
    public TextMeshProUGUI text;
    public float timeToFade;

    public int state;

    public GameObject rock;
    public float timeLeft;

    // Start is called before the first frame update
    void Start()
    {
        state = 1;
        text = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if (state == 3)
        {
            timeLeft = rock.GetComponent<KillingBoulder>().timeleft;
            text.SetText("Timeleft: " + Mathf.CeilToInt(timeLeft));
            return;
        }

        if (timeToFade <= 0)
        {
            Color color = text.color;

            if (color.a > 0)
            {
                text.color = new Color(color.r, color.g, color.b, color.a - Time.deltaTime);
            }
            else
            {
                if (state == 1)
                {
                    state = 2;
                    text.color = Color.red;
                    text.SetText("Squish them all!!!");
                    timeToFade = 2f;
                }
                else if (state == 2)
                {
                    state = 3;
                    text.color = Color.yellow;
                    
                }

            }
        }
        else
            timeToFade -= Time.deltaTime;

    }
}
