﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timeout : MonoBehaviour
{
    public float timeout;

    // Start is called before the first frame update
    void Start()
    {
        if (timeout == 0)
            timeout = 5f;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeout <= 0)
            Destroy(gameObject);
        else
            timeout -= Time.deltaTime;
    }

}
