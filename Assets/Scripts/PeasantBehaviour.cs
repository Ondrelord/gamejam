﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PeasantBehaviour : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
{
    [Header("Behaviour")]
    public GameObject zone;
    public float moveSpeed;

    private Rigidbody2D rb;
    public Vector2 target;
    public float moveTimout;

    public GameObject deathPrefab;
    private static bool applicationIsQuitting;

    [Header("Highlight")]
    public Canvas highlightCanvas;

    [Header("Dragging")]
    public bool isDragged;

    [Header("SpeechBubble")]
    public GameObject infoBubblePrefab;
    public GameObject info;

    // Start is called before the first frame update
    void Start()
    {
        if (zone == null)
            zone = transform.parent.gameObject;

        target = Vector2.zero;
        rb = GetComponent<Rigidbody2D>();

        //highlight
        highlightCanvas = GetComponentInChildren<Canvas>();
        highlightCanvas.worldCamera = Camera.main;
        highlightCanvas.enabled = false;
    }

    [RuntimeInitializeOnLoadMethod]
    static void RunOnStart()
    {
        Application.quitting += () => applicationIsQuitting = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isDragged)
            return;

        if (target == Vector2.zero)
        {
            Vector2 zoneExtents = zone.GetComponent<SpriteRenderer>().bounds.extents;
            Vector2 zoneCenter = zone.GetComponent<SpriteRenderer>().bounds.center;

            Vector2 rnd = Random.insideUnitCircle;
            target = new Vector2(zoneCenter.x + rnd.x * zoneExtents.x, zoneCenter.y + rnd.y * zoneExtents.y);

            moveTimout = Random.Range(2f,5f);
        }
        else if (Vector2.Distance(rb.position, target) < 0.35f || moveTimout <= 0)
        {
            target = Vector2.zero;
        }

        if (target != Vector2.zero)
        {
            moveTimout -= Time.deltaTime;
            rb.MovePosition(Vector2.Lerp(rb.position, target, (1f - Vector2.Distance(rb.position, target) / 100f) * moveSpeed));
        }
    }

    void OnDestroy()
    {
        if (!applicationIsQuitting)
            Instantiate(deathPrefab, rb.position, Random.rotation);
    }

    public void Highlight()
    {
        if (!highlightCanvas.enabled)
            highlightCanvas.enabled = true;
    }

    public void StopHighlight()
    {
        if (highlightCanvas.enabled)
            highlightCanvas.enabled = false;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            isDragged = true;

            GetComponent<Collider2D>().enabled = false;
            GetComponent<SpriteRenderer>().sortingLayerName = "DraggedObject";
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        CursorManager.instance.isPickUp = true;

        if (eventData.button == PointerEventData.InputButton.Left)
        {
            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            rb.MovePosition(Vector2.Lerp(rb.position, pos, 0.2f));
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            isDragged = false;

            //message
            //message = GetMessage(eventData.GetType, type);
            if (eventData.pointerEnter != null)
            {
                boxMsg msg; msg.text = ""; msg.title = "";
                ObjectType from, to;
                if (eventData.pointerDrag.TryGetComponent<ObjectType>(out from) && eventData.pointerEnter.TryGetComponent<ObjectType>(out to))
                    msg = GameObject.Find("Background").GetComponent<GameManager>().interaction(from.item, to.item);

                string messageTitle = msg.title;
                string messageText = msg.text;

                if (msg.title != "")
                {
                    Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    info = Instantiate(infoBubblePrefab, pos, Quaternion.identity);
                    info.transform.GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(messageTitle);
                    info.transform.GetChild(0).GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().SetText(messageText);
                    info.GetComponent<Canvas>().worldCamera = Camera.main;
                }
            }
            //draging
            GetComponent<SpriteRenderer>().sortingLayerName = "Default";
            GetComponent<Collider2D>().enabled = true;
        }

        CursorManager.instance.isPickUp = false;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            string messageTitle, messageText;
            if (!(GetComponent<ObjectType>().item == Item.Elder))
            {
                messageTitle = "Lord of the Cutlery";
                string tool = GetTool();
                if (tool != "")
                    messageText = "You have my " + tool + "!";
                else
                    messageText = "I'm not working for you!";
            }
            else
            {
                messageTitle = "We have a problem";
                messageText = GameObject.Find("Background").GetComponent<GameManager>().pManager.getActiveProblemsString();
            }


            Vector2 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            info = Instantiate(infoBubblePrefab, pos, Quaternion.identity);
            info.transform.GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().SetText(messageTitle);
            info.transform.GetChild(0).GetChild(1).GetComponent<TMPro.TextMeshProUGUI>().SetText(messageText);
            info.GetComponent<Canvas>().worldCamera = Camera.main;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            Destroy(info);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Highlight();

        if (!AudioManager.instance.isPlaying())
        {
            AudioManager.instance.RandomPitch();
            AudioManager.PlaySound(AudioManager.GetClips()[6]);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        StopHighlight();
    }

    public string GetTool()
    {
        switch (GetComponent<ObjectType>().item)
        {
            case Item.Axe:
                return "Axe";
            case Item.Elder:
                return "Wisdom";
            case Item.Fork:
                return "Fork";
            case Item.Knife:
                return "Knife";
            case Item.PickAxe:
                return "Pickaxe";
            case Item.Shovel:
                return "Shovel";
            case Item.Spoon:
                return "Spoon";
            default:
                return "";
        }
    }
}
