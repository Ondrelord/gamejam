﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventAudioManager : MonoBehaviour
{
    public AudioClip[] audioClips;
    public static EventAudioManager instance;

    private AudioSource player;

    private void Awake()
    {
        player = GetComponent<AudioSource>();
    }

    void Start()
    {
        if (instance == null) instance = this;

        DontDestroyOnLoad(gameObject); // Not destroyable
    }

    void Update()
    {
        if (!isPlaying())
            RestoreNormalPitch();
    }


    public void playSound(AudioClip clip)
    {
        player.clip = clip;
        player.Play();
    }

    public void playSoundOneShot(AudioClip clip)
    {
        player.clip = clip;
        player.PlayOneShot(clip);
    }

    public static AudioClip[] GetClips()
    {
        return instance.audioClips;
    }

    public static void PlaySound(AudioClip clip) // For not write AudioManager.insatance.playSound(clip) every;
    {
        instance.playSound(clip);
    }

    public static void PlaySoundOneShot(AudioClip clip)
    {
        instance.playSoundOneShot(clip);
    }

    public bool isPlaying()
    {
        if (player.isPlaying)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void RandomPitch()
    {
        float random_pitch = Random.Range(0.5f, 1.5f);
        player.pitch = random_pitch;
    }

    public void RestoreNormalPitch()
    {
        player.pitch = 1f;
    }

    public void ActivateLoop()
    {
        player.loop = true;
    }

    public void DeactivateLoop()
    {
        player.loop = false;
    }

    public void Silent()
    {
        player.Stop();
    }

    public void SetVolume(float volume)
    {
        player.volume = volume;
    }
}
