﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class VillageSound : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public void OnPointerEnter(PointerEventData eventData)
    {
        AudioManager.instance.SetVolume(0.1f);
        AudioManager.instance.ActivateLoop();
        AudioManager.PlaySound(AudioManager.GetClips()[11]);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        AudioManager.instance.SetVolume(0.5f);
        AudioManager.instance.DeactivateLoop();
        AudioManager.instance.Silent();
    }
}
