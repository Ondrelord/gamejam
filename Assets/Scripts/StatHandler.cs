﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StatHandler : MonoBehaviour
{
    public GameManager gameManager;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {
        winStats stats = gameManager.getWinStats();
        TextMeshProUGUI text = transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>();
        string done = "";

        if (stats.barbarians)   done = done + "Barbarians\n";
        if (stats.Cold)         done = done + "Cold\n";
        if (stats.dragon)       done = done + "Dragon\n";
        if (stats.fishing)      done = done + "Fishing\n";
        if (stats.Food)         done = done + "Food\n";
        if (stats.Security)     done = done + "Security\n";
        if (stats.sheep)        done = done + "Sheep\n";
        if (stats.tree)         done = done + "Tree\n";
        if (stats.Water)        done = done + "Water\n";
        if (stats.wolf)         done = done + "Wolf\n";

        text.SetText(done);
    }

}
