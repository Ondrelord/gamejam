﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ButtonHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public int state;

    [Header("Basic")]
    public TMP_FontAsset basicFont;
    public Color basicColor;
    public string basicText;

    [Header("Highlight")]
    public TMP_FontAsset highlightFont;
    public Color highlightColor;
    public string highlightText;

    public Transform bubble;

    void Start()
    {
        state = 1;
        basicText = highlightText = GetComponentInChildren<TextMeshProUGUI>().text;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerEnter.tag == "Button")
        {
            GetComponentInChildren<TextMeshProUGUI>().font = highlightFont;
            GetComponentInChildren<TextMeshProUGUI>().color = highlightColor;
            GetComponentInChildren<TextMeshProUGUI>().SetText(highlightText);
            if (bubble != null) bubble.localScale = new Vector3(-1, 1, 1);

            AudioManager.PlaySound(AudioManager.GetClips()[1]);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (eventData.pointerEnter.tag == "Button")
        {
            GetComponentInChildren<TextMeshProUGUI>().font = basicFont;
            GetComponentInChildren<TextMeshProUGUI>().color = basicColor;
            GetComponentInChildren<TextMeshProUGUI>().SetText(basicText);
            if (bubble != null) bubble.localScale = new Vector3(1, 1, 1);
        }
    }

    public void NewGameButtonSequence()
    {
        switch(state)
        {
            case 1:
                highlightText = "Are you sure?";
                GetComponentInChildren<TextMeshProUGUI>().SetText(highlightText);
                state++;
                break;
            case 2:
                basicText = "Yes, I'm sure";
                highlightText = "I don't think so";
                GetComponentInChildren<TextMeshProUGUI>().SetText(highlightText);
                state++;
                break;
            case 3:
                basicText = "I prommise, it's fun";
                highlightText = "Try the other button";
                GetComponentInChildren<TextMeshProUGUI>().SetText(highlightText);
                state++;
                break;
            case 4:
                basicText = "Ok, Ok, here it comes";
                highlightText = "just one more time";
                GetComponentInChildren<TextMeshProUGUI>().SetText(highlightText);
                state++;
                break;
            case 5:
                basicText = "Start the Game";
                highlightText = "But I warned you";
                GetComponentInChildren<TextMeshProUGUI>().SetText(highlightText);
                state++;
                break;
            case 6:
                basicText = "New Game";
                highlightText = "New Game";
                SceneManager.LoadScene(1);
                state = 1;
                break;
        }
    }
}
