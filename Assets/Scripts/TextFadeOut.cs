﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextFadeOut : MonoBehaviour
{
    public TextMeshProUGUI text;
    public float timeToFade;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        if (timeToFade <= 0)
        {
            Color color = text.color;
            if (color.a > 0)
                text.color = new Color(color.r, color.g, color.b, color.a - Time.deltaTime);
            else
                gameObject.active = false;
        }
        else
            timeToFade -= Time.deltaTime;

    }
}
